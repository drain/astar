#include "iostream"
#include "MapUtility.h"

using namespace std;

int main()
{
	MapUtility mapUtil("map.txt");
	mapUtil.PrintMap();

	cout << endl;

	vector<Node> path = mapUtil.GetPath(0, 0, 10, 8);
	mapUtil.PrintPath(path);

	cout << endl;

	vector<Node> path1 = mapUtil.GetPath(10, 8, 6, 5);
	mapUtil.PrintPath(path1);

	cout << endl;

	vector<Node> path2 = mapUtil.GetPath(0, 8, 10, 1);
	mapUtil.PrintPath(path2);

	system("pause");
	return 0;
}