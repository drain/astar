#include "MapUtility.h"


MapUtility::MapUtility(std::string path)
{
	//read map and save it
	std::ifstream temp(path);
	std::stringstream buffer;
	buffer << temp.rdbuf();

	std::string mapData = buffer.str();

	w = std::stoi(mapData.substr(0, mapData.find(",")));
	h = std::stoi(mapData.substr(mapData.find(",") + 1, mapData.find("\n")));

	map = new Node*[w];
	for (int i = 0; i < w; i++)
		map[i] = new Node[h];

	int wTemp = 0;
	int hTemp = 0;
	
	//skip the first row
	int skip = (mapData.substr(0, mapData.find("\n")).size() / sizeof(char)) + 1;

	for (int i = skip; i < mapData.size(); i++)
	{
		if (mapData[i] == '\n')
		{
			hTemp++;
			wTemp = 0;
			i++;
		}
		//we assume that there cant be more than 1 whitespace between numbers
		else if (mapData[i] == ' ')
		{
			i++;
		}
		
		//assign node values
		map[wTemp][hTemp].moveVal = atoi(&mapData[i]);
		map[wTemp][hTemp].x = wTemp;
		map[wTemp][hTemp].y = hTemp;

		wTemp++;
	}
}

MapUtility::~MapUtility()
{
	if (map != nullptr)
	{
		for (int i = 0; i < h; i++)
			delete[] map[i];
		delete[] map;
	}
}

//returns path from x0,y0 to x1,y1
std::vector<Node> MapUtility::GetPath(int x0, int y0, int x1, int y1)
{
	//we are at the point already
	if (x0 == x1 && y0 == y1)
		return std::vector<Node>();

	std::vector<Node> pathToReturn;

	//start and end points
	Node start(x0, y0);
	Node end(x1, y1);

	//set start f score to manhattan distance from start to end
	start.f_score = fabs(start.x - end.x) + fabs(start.y - end.y);

	//lists of open and closed nodes
	std::vector<Node*> open; 
	std::vector<Node*> closed;

	//Add starting point
	open.push_back(&start); 

	//loop untill no open nodes left to check
	while (open.size() > 0)
	{
		//Get node with lowest F value
		Node* curNode = getBestNode(open);

		//if we are at end position
		if (curNode->x == end.x && curNode->y == end.y)
		{
			//reconstruct path here
			Node n = *curNode;
			pathToReturn.push_back(n);
			while (n.parent != &start)
			{
				n = *n.parent;
				pathToReturn.push_back(n);
			}

			pathToReturn.push_back(start);
			std::reverse(pathToReturn.begin(), pathToReturn.end());
			break;
		}

		//add curnode to closed and remove it from open
		closed.push_back(curNode);
		std::vector<Node*>::iterator it = std::find(open.begin(), open.end(), curNode);
		open.erase(it);

		//get neighbours for currentNode
		std::vector<Node*> neighbors = FindValidFourNeighbours(curNode);

		//check neighbors for best f_score
		for(int i = 0; i < neighbors.size(); i++)
		{
			float G = curNode->G + 1;
			float H = fabs(neighbors[i]->x - end.x) + fabs(neighbors[i]->y - end.y);
			//add move value into calculation
			float f_score = G + H + curNode->moveVal;
			
			//skip higher scored
			if (isValueInList(neighbors[i], closed) && f_score >= neighbors[i]->f_score)
				continue;

			//skip walls
			if (neighbors[i]->moveVal == 0)
				continue;

			//if score < then add to open, if its not there already
			if (!isValueInList(neighbors[i], open) || f_score < neighbors[i]->f_score)
			{
				//set parent to best f_score neighbor
				neighbors[i]->parent = curNode;
				neighbors[i]->G = G;
				neighbors[i]->H = H;
				//add move value into calculation
				neighbors[i]->f_score = G + H + curNode->moveVal;

				if (!isValueInList(neighbors[i], open))
				{
					open.push_back(neighbors[i]);
				}
			}
		}
	}

	return pathToReturn;
}

//prints the map
void MapUtility::PrintMap()
{
	if (map != nullptr)
	{
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
				std::cout << ((map[j][i].moveVal == 0) ? "W" : ((map[j][i].moveVal > 1) ? "-" : "_")) << " ";
			std::cout << std::endl;
		}
	}
}

//check if path has the specific node in it (used for printing)
bool pathHasNode(Node n, std::vector<Node> path)
{
	for (int k = 0; k < path.size(); k++)
	{
		if (path[k].x == n.x && path[k].y == n.y)
			return true;
	}

	return false;
}

//prints path to our map
void MapUtility::PrintPath(std::vector<Node> path)
{
	if (map != nullptr)
	{
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (pathHasNode(map[j][i], path))
				{
					if(map[j][i].x == path[0].x && map[j][i].y == path[0].y)
						std::cout << "S" << " ";
					else if(map[j][i].x == path[path.size()-1].x && map[j][i].y == path[path.size() - 1].y)
						std::cout << "E" << " ";
					else
						std::cout << "#" << " ";
				}
					
				else
					std::cout << ((map[j][i].moveVal == 0) ? "W" : ((map[j][i].moveVal > 1) ? "-" : "_")) << " ";
			}
				
			std::cout << std::endl;
		}
	}
}

//returns node from map[x][y], returns nullptr if not found
Node* MapUtility::getNode(int x, int y)
{
	return (x >= 0 && x < w && y >= 0 && y < h) ? &map[x][y] : nullptr;
}

//Gets best f_score node out of nodelist
Node* MapUtility::getBestNode(std::vector<Node*> nodes)
{
	if (nodes.size() > 0)
	{
		Node* best = nodes[0];
		for (int i = 0; i < nodes.size(); i++)
			if (nodes[i]->f_score < best->f_score)
				best = nodes[i];
		return best;
	}
	else
		return nullptr;
}

//check if node exists in list
bool MapUtility::isValueInList(Node* node, std::vector<Node*> map)
{
	bool rVal = false;

	for (int i = 0; i < map.size(); i++)
		if (map[i] == node)
			rVal = true;
	return rVal;
}

//Find neighbours for the given node (up,down,left,right)
std::vector<Node*> MapUtility::FindValidFourNeighbours(Node* node)
{
	std::vector<Node*> neighbours;
	Node* n;

	//one node down
	n = getNode(node->x, node->y - 1);
	if (n != nullptr)
		neighbours.push_back(n);

	//one node up
	n = getNode(node->x, node->y + 1);
	if (n != nullptr)
		neighbours.push_back(n);

	//one node to left
	n = getNode(node->x - 1, node->y);
	if (n != nullptr)
		neighbours.push_back(n);

	//one node to right
	n = getNode(node->x + 1, node->y);
	if (n != nullptr)
		neighbours.push_back(n);

	return neighbours;
}

