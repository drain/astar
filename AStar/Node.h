#pragma once

#include "vector"

class Node
{
public:
	Node(int x, int y);
	Node();
	~Node();

	int x;
	int y;
	int moveVal;

	float f_score;
	float G;
	float H;
	Node* parent = nullptr;
	std::vector<Node*> neighbors;
};

