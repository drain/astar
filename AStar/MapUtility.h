#pragma once

#include "string"
#include "fstream"
#include "sstream"
#include "iostream"
#include "Node.h"
#include "math.h"
#include "vector"

class MapUtility
{
private:
	Node** map = nullptr;
	int w, h;
	std::vector<Node*> FindValidFourNeighbours(Node* node);
	bool isValueInList(Node* node, std::vector<Node*> map);
	Node* getBestNode(std::vector<Node*> nodes);
public:
	MapUtility(std::string path);
	~MapUtility();
	
	std::vector<Node> GetPath(int x0, int y0, int x1, int y1);
	void PrintPath(std::vector<Node> path);
	void PrintMap();
	
	Node* getNode(int x, int y);
};

